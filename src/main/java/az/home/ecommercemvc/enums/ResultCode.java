package az.home.ecommercemvc.enums;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;

@AllArgsConstructor
@Getter
public enum  ResultCode {

    OK(1,"OK"),
    DATA_NOT_FOUND(-2,"data not found"),
    SUCCESS(1,"success");
    private int code;
    private String message;

}
