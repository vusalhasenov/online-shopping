package az.home.ecommercemvc.model;

import az.home.ecommercemvc.constants.AppConstants;
import az.home.ecommercemvc.enums.ResultCode;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.time.LocalDateTime;

@Data
public class ApiResult {

    private final int code;
    private final String message;
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = AppConstants.DATE_TIME_FORMAT)
    private LocalDateTime timeStamp;

    public ApiResult(ResultCode resultCode) {
        this.code = resultCode.getCode();
        this.message = resultCode.getMessage();
        this.timeStamp = LocalDateTime.now();
    }


    public ApiResult(int resultCode, String message) {
        this.code=resultCode;
        this.message=message;
        this.timeStamp = LocalDateTime.now();

    }



}

