package az.home.ecommercemvc.model;

import az.home.ecommercemvc.enums.ResultCode;
import lombok.Getter;

import java.time.LocalDateTime;
import java.util.List;

@Getter
public class ListResult<M> extends ApiResult{

    private List<M> data;

    public ListResult(ResultCode resultCode, List<M> data) {
        super(resultCode);
        this.data = data;
    }

    public ListResult(List<M> data){
        super(ResultCode.OK);
        this.data=data;
    }
}
