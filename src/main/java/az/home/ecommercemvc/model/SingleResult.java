package az.home.ecommercemvc.model;

import az.home.ecommercemvc.enums.ResultCode;
import lombok.Getter;

@Getter
public class SingleResult<M> extends ApiResult {

    private M data;

    public SingleResult(ResultCode resultCode, M data) {
        super(resultCode);
        this.data = data;
    }

    public SingleResult(M data) {
        super(ResultCode.SUCCESS);
        this.data = data;
    }
}
