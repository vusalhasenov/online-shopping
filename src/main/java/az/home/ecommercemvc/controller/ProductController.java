package az.home.ecommercemvc.controller;


import az.home.ecommercemvc.dto.ProductDto;
import az.home.ecommercemvc.entity.Product;
import az.home.ecommercemvc.model.ListResult;
import az.home.ecommercemvc.model.SingleResult;
import az.home.ecommercemvc.service.ProductService;
import az.home.ecommercemvc.service.impl.ProductServiceImpl;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/products")
public class ProductController {

    public final ProductService productService;

    @GetMapping()
    public List<ProductDto> findAll(){
        return productService.all();
    }

    @GetMapping("/{id}")
    public SingleResult findById(@PathVariable("id") Long id){
        return new SingleResult(productService.findById(id));
    }
}
