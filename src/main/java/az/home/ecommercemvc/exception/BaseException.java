package az.home.ecommercemvc.exception;

import az.home.ecommercemvc.enums.ResultCode;
import lombok.Getter;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
@Getter
public class BaseException extends RuntimeException{
    private final ResultCode resultCode;


}
