package az.home.ecommercemvc.exception;

import az.home.ecommercemvc.model.ApiResult;
import az.home.ecommercemvc.model.ListResult;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

@RestControllerAdvice
public class GlobalExceptionHandler {


    @ExceptionHandler(BaseException.class)
    private ApiResult dataNotFound(BaseException baseException){
        return new ApiResult(baseException.getResultCode());
    }
}
