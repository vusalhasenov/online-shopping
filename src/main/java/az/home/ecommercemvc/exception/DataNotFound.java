package az.home.ecommercemvc.exception;

import az.home.ecommercemvc.enums.ResultCode;
import lombok.Getter;

@Getter
public class DataNotFound extends BaseException {

    public DataNotFound(ResultCode resultCode) {
        super(resultCode);
    }

    public static DataNotFound dataNotFound(){
        return new DataNotFound(ResultCode.DATA_NOT_FOUND);
    }
}
