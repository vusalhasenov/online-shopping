package az.home.ecommercemvc.constants;


import lombok.AccessLevel;
import lombok.NoArgsConstructor;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class AppConstants {

    public static final String DATE_TIME_FORMAT = "dd.MM.yyyy HH:mm:ss";

}
