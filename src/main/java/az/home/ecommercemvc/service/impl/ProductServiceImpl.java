package az.home.ecommercemvc.service.impl;

import az.home.ecommercemvc.dto.ProductDto;
import az.home.ecommercemvc.entity.Category;
import az.home.ecommercemvc.entity.Product;
import az.home.ecommercemvc.exception.DataNotFound;
import az.home.ecommercemvc.repo.ProductRepo;
import az.home.ecommercemvc.service.ProductService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@RequiredArgsConstructor
@Service
public class ProductServiceImpl implements ProductService {

    public final ProductRepo productRepo;
    @Override
    public List<ProductDto> all() {
        return productRepo.findAll().stream().map(product -> mapToDto(product))
                .collect(Collectors.toList());
    }

    @Override
    public ProductDto findById(Long id) {
        return productRepo.findById(id).map(product ->mapToDto(product))
                .orElseThrow(() -> DataNotFound.dataNotFound());
    }

    private ProductDto mapToDto(Product product){
        ProductDto productDto = new ProductDto();
        productDto.setId(product.getId());
        productDto.setImage(product.getImage());
        productDto.setName(product.getProductName());
        productDto.setDisplay(product.getDisplay());
        productDto.setMemory(product.getMemory());
        productDto.setRam(product.getRam());
        productDto.setCategoryId(product.getCategoryId().getName());
        productDto.setIsActive(product.getIsActive());
        productDto.setAvailable(product.getAvailable());
        productDto.setSeller(product.getSeller());
        productDto.setPrice(product.getPrice());
        return productDto;
    }
}
