package az.home.ecommercemvc.service;

import az.home.ecommercemvc.dto.ProductDto;
import az.home.ecommercemvc.entity.Product;

import java.util.List;

public interface ProductService {

    List<ProductDto> all();

    ProductDto findById(Long id);
}
