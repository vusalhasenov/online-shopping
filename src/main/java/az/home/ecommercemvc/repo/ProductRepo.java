package az.home.ecommercemvc.repo;

import az.home.ecommercemvc.entity.Product;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface ProductRepo extends CrudRepository<Product,Long> {
    List<Product> findAll();
}
