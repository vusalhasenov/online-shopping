package az.home.ecommercemvc.dto;

import az.home.ecommercemvc.entity.Category;
import lombok.Data;
import lombok.NoArgsConstructor;


@Data
@NoArgsConstructor

public class ProductDto {

    private Long id;
    private String name;
    private String display;
    private Double price;
    private String ram;
    private String memory;
    private String code;
    private String categoryId;
    private Integer isActive;
    private String image;
    private Integer available;
    private String seller;

}
