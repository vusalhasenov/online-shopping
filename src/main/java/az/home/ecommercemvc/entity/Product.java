package az.home.ecommercemvc.entity;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.annotation.processing.Generated;
import javax.naming.Name;
import javax.persistence.*;

@Entity
@Data
@NoArgsConstructor
@Table(name = "product")
public class Product {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @Column(name = "name")
    private String productName;

    @Column(name = "seller")
    private String seller;

    @Column(name = "price")
    private Double price;

    @Column(name = "available")
    private Integer available;

    @Column(name = "image")
    private String image;

    @Column(name = "display")
    private String display;

    @Column(name = "ram")
    private String ram;

    @Column(name = "memory")
    private String memory;

//    @Column(name = "quantity")
//    private Double quantity;

    @Column(name = "code")
    private String code;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "category_id")
    private Category categoryId;

    @Column(name = "is_active")
    private Integer isActive;

//    'id':'',
//            'image':'https://productimages.hepsiburada.net/s/32/500/10352568139826.jpg',
//            'name':'Product 1',
//            'display':'5 inch',
//            'ram':'4GB',
//            'memory':'32GB',
//            'quantity':2,
//            'avilable':22,
//            'deliveryIn':'In 3 Days',
//            'price':500,
//            'monney':'AZN',
//            'category':''
//

}
